CREATE TABLE room (
    room_id TEXT PRIMARY KEY,
    name TEXT NOT NULL,
    topic TEXT,
    world_readable BOOLEAN NOT NULL,
    guests_can_join BOOLEAN NOT NULL,
    avatar_url TEXT,
    is_local BOOLEAN NOT NULL,
);

CREATE TABLE room_alias (
    alias TEXT PRIMARY KEY,
    room_id TEXT NOT NULL REFERENCES room(room_id);,
);

CREATE TABLE room_member (
    room_id NOT NULL REFERENCES room(room_id);
    user_id NOT NULL REFERENCES user(user_id);
);

CREATE TABLE user (
    user_id TEXT NOT NULL,
    homeserver TEXT NOT NULL,
    last_seen DATETIME NOT NULL,
    avatar_url TEXT,
    display_name TEXT
);

CREATE TABLE account (
    user_id
    avatar_url
    display_name
);

CREATE TABLE account_config (
    user_id TEXT NOT NULL REFERENCES account(user_id);,
);

CREATE TABLE account_device (
    user_id TEXT NOT NULL REFERENCES account(user_id);,
);

CREATE TABLE account_token (
    user_id TEXT NOT NULL REFERENCES account(user_id);,
);

CREATE TABLE media (
);

CREATE TABLE homeserver (
    fqdn TEXT NOT NULL,
);

CREATE TABLE pdu (
    room_id TEXT PRIMARY NOT NULL,
    sender TEXT NOT NULL,
    origin TEXT NOT NULL,
    origin_server_ts TEXT,
    type
    state_key
    content
    prev_events
    depth
    auth_events
    redacts
    unsigned
    hashes
    signatures
);
