use std::convert::Infallible;

use sqlx::SqlitePool;
use warp::{Filter, Reply, Rejection};

use crate::config::Config;

pub mod error;
pub mod auth;

pub mod login;
pub mod register;

fn api_prefix() -> impl Filter<Extract=(), Error=Rejection> + Clone {
    warp::path!("_matrix" / "_client" / "r0" / ..)
}

// pool: sqlx::Pool<impl sqlx::Connect>
//
pub fn routes(config: &Config, pool: &SqlitePool) -> impl Filter<Extract=impl Reply, Error=Infallible> + Clone {
    login::routes(config, pool)
        .recover(error::handle_rejection)
}
