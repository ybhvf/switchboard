use warp::{Filter, reject::Rejection};

use crate::db;
use crate::api::client_server::r0_6_1::error::{ErrorCode, api_error, StdErrHandler};

pub(crate) fn auth_check(pool: &sqlx::SqlitePool) -> impl Filter<Extract=(db::Device,), Error=Rejection> + Clone {
    async fn check_token(pool: sqlx::SqlitePool, token: Option<String>) -> Result<db::Device, Rejection> {
        if let Some(tok) = token {
            db::Device::find_by_token(&pool, &tok).await
                .std_api_err()?
                .ok_or(api_error(ErrorCode::UnknownToken, None))
        } else {
            Err(api_error(ErrorCode::MissingToken, None))
        }
    }

    let pool = pool.clone();
    // TODO: also check query params
    warp::any().map(move || pool.clone())
        .and(warp::header::optional::<String>("Authorization"))
        .and_then(check_token)
}
