use std::fmt;

pub struct User {
    pub user_id: String,
    pub homeserver: String,
    pub identity_server: String,

    pub last_seen: i32,
    pub avatar_url: String,
    pub display_name: String,
}

#[derive(Debug)]
pub enum Error {
    SqlxError(sqlx::error::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "database error")
    }
}
impl std::error::Error for Error {}

impl User {
    pub async fn find_by_id(pool: &sqlx::SqlitePool, user_id: &str) -> Result<Option<User>, Error> {
        let mut conn = pool.acquire().await.map_err(Error::SqlxError)?;

        match sqlx::query_as!(User,
                "SELECT * FROM user WHERE user_id = ?",
                user_id)
            .fetch_one(&mut conn)
            .await {
                Ok(val) => Ok(Some(val)),
                Err(x) => match x {
                    sqlx::Error::RowNotFound => Ok(None),
                    _ => Err(Error::SqlxError(x))
                }
            }
    }

    pub fn matrix_id(&self) -> super::identity::MatrixId {
        super::identity::MatrixId::Full( super::identity::FullId {
            local: super::identity::LocalId { id: self.user_id.clone() },
            domain: serde_json::from_str(&self.homeserver).expect("homeserver in db should be valid")
        })
    }

    // pub async fn account(&self, pool: &sqlx::SqlitePool) -> Result<Account, Error> { None }
}

pub struct Account {
    pub user_id: String,
    scrypt_key: String,
}

impl Account {
    pub async fn new(pool: &sqlx::SqlitePool, device_id: &str, user_id: &str, password: &str) -> Result<Account, Error> {
        let mut conn = pool.acquire().await.map_err(Error::SqlxError)?;

        // TODO (ljencka): return error maybe?
        let hash = crate::auth::hash_password(password);

        let account = Account {
            user_id: user_id.to_string(),
            scrypt_key: hash,
        };

        sqlx::query!("INSERT INTO account VALUES (?, ?)",
                     account.user_id, account.scrypt_key)
            .execute(&mut conn)
            .await
            .map_err(Error::SqlxError)?;

        Ok(account)
    }

    pub async fn find_by_id(pool: &sqlx::SqlitePool, user_id: &str) -> Result<Option<Account>, Error> {
        let mut conn = pool.acquire().await.map_err(Error::SqlxError)?;

        match sqlx::query_as!(Account,
                "SELECT * FROM account WHERE user_id = ?",
                user_id)
            .fetch_one(&mut conn)
            .await {
                Ok(val) => Ok(Some(val)),
                Err(x) => match x {
                    sqlx::Error::RowNotFound => Ok(None),
                    _ => Err(Error::SqlxError(x))
                }
            }
    }

    pub async fn user(&self, pool: &sqlx::SqlitePool) -> Result<User, Error> {
        let user = User::find_by_id(pool, &self.user_id)
            .await?
            .expect(&format!("user '{}' should exist in db", &self.user_id));
        Ok(user)
    }

    pub async fn devices(&self, pool: &sqlx::SqlitePool) -> Result<Vec<Device>, Error> {
        Device::find_by_user(pool, &self.user_id).await
    }

    pub async fn device(&self, pool: &sqlx::SqlitePool, device_id: &str) -> Result<Device, Error> {
        let device = self.devices(pool).await?
            .into_iter().find(|dev| dev.device_id == device_id);

        match device {
            None => Device::new(pool, device_id, &self.user_id).await,
            Some(dev) => Ok(dev)
        }
    }

    pub fn check_password(&self, password: &str) -> bool {
        return scrypt::scrypt_check(password, &self.scrypt_key).is_ok();
    }

    pub async fn set_password(&mut self, pool: &sqlx::SqlitePool, password: &str) -> Result<(), Box<dyn std::error::Error>>{
        let hash = crate::auth::hash_password(password);

        let mut conn = pool.acquire().await?;
        sqlx::query!("UPDATE account SET scrypt_key = ? WHERE user_id = ?",
                     hash, &self.user_id)
            .execute(&mut conn)
            .await?;

        self.scrypt_key = hash;

        Ok(())
    }
}

pub struct Device {
    pub device_id: String,
    pub user_id: String,
    pub last_seen: i32,
    token: String,
}

impl Device {
    pub async fn new(pool: &sqlx::SqlitePool, device_id: &str, user_id: &str) -> Result<Device, Error> {
        let mut conn = pool.acquire().await.map_err(Error::SqlxError)?;

        let device = Device {
            device_id: device_id.to_string(),
            user_id: user_id.to_string(),
            last_seen: 0,
            token: crate::auth::gen_token()
        };

        sqlx::query!("INSERT INTO account_device VALUES (?, ?, ?, ?)",
                     device.device_id, device.user_id, device.last_seen, device.token)
            .execute(&mut conn)
            .await
            .map_err(Error::SqlxError)?;

        Ok(device)
    }

    pub async fn delete(&self, pool: &sqlx::SqlitePool) -> Result<(), Error> {
        let mut conn = pool.acquire().await.map_err(Error::SqlxError)?;

        sqlx::query!("DELETE FROM account_device WHERE user_id = ? AND device_id = ?",
                     self.user_id, self.device_id)
            .execute(&mut conn)
            .await
            .map_err(Error::SqlxError)?;

        Ok(())
    }

    pub async fn account(&self, pool: &sqlx::SqlitePool) -> Result<Account, Error> {
        let acct = Account::find_by_id(pool, &self.user_id)
            .await?
            .expect(&format!("account '{}' should exist in db", &self.user_id));
        Ok(acct)
    }

    pub async fn user(&self, pool: &sqlx::SqlitePool) -> Result<User, Error> {
        let user = User::find_by_id(pool, &self.user_id)
            .await?
            .expect(&format!("user '{}' should exist in db", &self.user_id));
        Ok(user)
    }

    pub async fn find_by_user(pool: &sqlx::SqlitePool, user_id: &str) -> Result<Vec<Device>, Error> {
        let mut conn = pool.acquire().await.map_err(Error::SqlxError)?;

        match sqlx::query_as!(Device,
                "SELECT * FROM account_device WHERE user_id=?",
                user_id)
            .fetch_all(&mut conn)
            .await {
                Ok(val) => Ok(val),
                Err(x) => match x {
                    sqlx::Error::RowNotFound => Ok(vec![]),
                    _ => Err(Error::SqlxError(x))
                }
            }
    }

    pub async fn find_by_token(pool: &sqlx::SqlitePool, token: &str) -> Result<Option<Device>, Error> {
        let mut conn = pool.acquire().await.map_err(Error::SqlxError)?;

        match sqlx::query_as!(Device,
                "SELECT * FROM account_device WHERE token = ?",
                token)
            .fetch_one(&mut conn)
            .await {
                Ok(val) => Ok(Some(val)),
                Err(x) => match x {
                    sqlx::Error::RowNotFound => Ok(None),
                    _ => Err(Error::SqlxError(x))
                }
            }
    }

    pub fn token(&self) -> String { self.token.clone() }

    pub async fn set_token(&mut self, pool: &sqlx::SqlitePool, new_token: &str) -> Result<(), Error> {
        let mut conn = pool.acquire().await.map_err(Error::SqlxError)?;

        sqlx::query!("UPDATE account_device SET token = ? WHERE user_id = ? AND device_id = ?",
                     new_token, &self.user_id, &self.device_id)
            .execute(&mut conn)
            .await
            .map_err(Error::SqlxError)?;

        // FIXME
        self.token = String::from(new_token);

        Ok(())
    }
}
