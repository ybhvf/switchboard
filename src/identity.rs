use std::fmt;
use regex::Regex;

use serde::{Serialize,Deserialize};

#[derive(Debug, Clone, PartialEq)]
pub struct DnsAddr {
    pub hostname: String,
    pub port: Option<u16>
}

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
#[serde(untagged)]
pub enum Domain {
    Ipv4Addr(std::net::Ipv4Addr),
    Ipv6Addr(std::net::Ipv6Addr),
    SocketAddrV4(std::net::SocketAddrV4),
    SocketAddrV6(std::net::SocketAddrV6),
    #[serde(with = "serde_with::rust::display_fromstr")]
    DnsAddr(DnsAddr)
}

#[derive(Debug, Clone, PartialEq)]
pub struct LocalId {
    pub id: String,
}

#[derive(Debug, Clone, PartialEq)]
pub struct FullId {
    pub local: LocalId,
    pub domain: Domain,
}

#[derive(Debug, Clone, PartialEq)]
#[derive(Deserialize, Serialize)]
#[serde(untagged)]
pub enum MatrixId {
    #[serde(with = "serde_with::rust::display_fromstr")]
    Full(FullId),
    #[serde(with = "serde_with::rust::display_fromstr")]
    Local(LocalId)
}

impl fmt::Display for DnsAddr {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.hostname)?;
        if let Some(port) = self.port {
            write!(f, ":{}", port)?
        }
        Ok(())
    }
}

impl fmt::Display for LocalId {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.id)
    }
}

impl fmt::Display for FullId {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        let domain_json = serde_json::to_value(&self.domain)
            .expect("domain must be parsable by serde");
        let domain = domain_json.as_str().expect("domain must serialize");
        write!(f, "@{}:{}", self.local.id, domain)
    }
}

impl fmt::Display for MatrixId {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        match self {
            MatrixId::Full(id) => id.fmt(f),
            MatrixId::Local(id) => id.fmt(f)
        }
    }
}

#[derive(Debug, Clone)]
pub struct ParseError;
impl fmt::Display for ParseError {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "identity is malformed")
    }
}
impl std::error::Error for ParseError {}

impl std::str::FromStr for DnsAddr {
    type Err = ParseError;

    fn from_str(value: &str) -> Result<DnsAddr, Self::Err> {
        let mut hostname = value;
        let mut port = None;

        if let Some(index) = value.find(':') {
            hostname = &value[..index];
            port = Some(str::parse::<u16>(&value[index+1..])
                .map_or(Err(ParseError), Ok)?);
        }

        if !publicsuffix::Domain::has_valid_syntax(hostname) {
            return Err(ParseError);
        }

        Ok(DnsAddr { hostname: hostname.to_string(), port: port })
    }
}

impl std::str::FromStr for LocalId {
    type Err = ParseError;

    fn from_str(value: &str) -> Result<LocalId, Self::Err> {
        // TODO (ljencka): be more conservative - assume X chars for domain
        if value.len() < 1 || value.len() > 250 {
            return Err(ParseError);
        }

        let localpart_re = Regex::new(r"^[\x21-\x39\x3b-\x7e]+$").expect("regex must compile");

        let value = value.trim();

        let id = localpart_re.find(value)
            .ok_or(ParseError)?.as_str().to_lowercase();

        Ok(LocalId { id: id })
    }
}

impl std::str::FromStr for FullId {
    type Err = ParseError;

    fn from_str(value: &str) -> Result<FullId, Self::Err> {
        use serde_json::{to_value, from_value};

        // shortest possible full mxid: @a:b.c
        if value.len() < 6 || value.len() > 255 {
            return Err(ParseError);
        }

        let mxid_re = Regex::new(r"^@?([\x21-\x39\x3b-\x7e]+):(.+)$").expect("regex must compile");

        let value = value.trim();
        let captures = mxid_re.captures(value).ok_or(ParseError)?;

        let id = captures.get(1).ok_or(ParseError)?.as_str().to_lowercase();

        let domain_str = captures.get(2).ok_or(ParseError)?.as_str();
        let domain_json = to_value(domain_str).map_or(Err(ParseError), Ok)?;
        let domain: Domain = from_value(domain_json).map_or(Err(ParseError), Ok)?;

        Ok(FullId { local: LocalId { id: id }, domain: domain })
    }
}

impl LocalId {
    pub fn to_full_id(&self, domain: Domain) -> FullId {
        FullId {
            local: self.clone(),
            domain: domain
        }
    }
    pub fn is_adherent(&self) -> bool {
        Regex::new(r"^[A-Za-z0-9-.=_/]+$").expect("regex must compile")
            .is_match(&self.id)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_local_id() {
        str::parse::<LocalId>("UsEr0").expect("should parse");
        str::parse::<LocalId>("!\"#$%&\'()*+,-./0123456789;<=>?@ABCDEFGHIJKLMNO\
        PQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}")
            .expect("historical ids should parse");
        str::parse::<LocalId>("@cool:user").expect_err("colons should not parse");
        str::parse::<LocalId>("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\
            aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\
            aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\
            aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\
            aaaaaa").expect_err("should not parse very long ids");
    }

    #[test]
    fn test_dns_addr() {
        let addr = str::parse::<DnsAddr>("some.gtld").expect("should parse");
        assert_eq!(addr.hostname, "some.gtld");

        let addr = str::parse::<DnsAddr>("some.gtld:80").expect("should parse");
        assert_eq!(addr.hostname, "some.gtld");
        assert_eq!(addr.port, Some(80));

        str::parse::<DnsAddr>("some..gtld").expect_err("should not parse");
        str::parse::<DnsAddr>("some.gtld:9999999").expect_err("should not parse");
    }

    #[test]
    fn test_domain() {
        use std::net::{Ipv4Addr, Ipv6Addr, SocketAddrV4, SocketAddrV6};

        let dom: Domain = serde_json::from_str("\"some.gtld:80\"")
            .expect("dns + port should parse");
        let addr = match dom {
            Domain::DnsAddr(addr) => addr,
            _ => panic!("should parse as DnsAddr")
        };
        assert_eq!(addr.hostname, "some.gtld");
        assert_eq!(addr.port, Some(80));

        let dom: Domain = serde_json::from_str("\"127.0.0.1\"")
            .expect("ipv4 should parse");
        let addr = match dom {
            Domain::Ipv4Addr(addr) => addr,
            _ => panic!("should parse as Ipv4Addr")
        };
        assert_eq!(addr, Ipv4Addr::LOCALHOST);

        let dom: Domain = serde_json::from_str("\"127.0.0.1:90\"")
            .expect("ipv4 + port should parse");
        let addr = match dom {
            Domain::SocketAddrV4(addr) => addr,
            _ => panic!("should parse as SocketAddrV4")
        };
        assert_eq!(addr, SocketAddrV4::new(Ipv4Addr::LOCALHOST, 90));

        let dom: Domain = serde_json::from_str("\"::1\"")
            .expect("ipv6 should parse");
        let addr = match dom {
            Domain::Ipv6Addr(addr) => addr,
            _ => panic!("should parse as Ipv4Addr")
        };
        assert_eq!(addr, Ipv6Addr::LOCALHOST);

        let dom: Domain = serde_json::from_str("\"[::1]:72\"")
            .expect("ipv6 + port should parse");
        let addr = match dom {
            Domain::SocketAddrV6(addr) => addr,
            _ => panic!("should parse as SocketAddrV6")
        };
        assert_eq!(addr, SocketAddrV6::new(Ipv6Addr::LOCALHOST, 72, 0, 0));
    }

    #[test]
    fn test_full_id() {
        str::parse::<FullId>("@user0:some.gtld").expect("should parse");
        str::parse::<FullId>("user0").expect_err("local id should not parse");
        str::parse::<FullId>("@!\"#$%&\'()*+,-./0123456789;<=>?@ABCDEFGHIJKLMN\
        OPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}:some.gtld")
            .expect("historical ids should parse");
        str::parse::<FullId>("@cool:user:some.gtld")
            .expect_err("username colons should not parse");
        str::parse::<FullId>("@aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\
            aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\
            aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\
            aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa:som\
            ee.gtld").expect_err("should not parse very long ids");
    }

    #[test]
    fn test_matrix_id() {
    }
}
