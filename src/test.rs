pub(crate) async fn fake_db() -> sqlx::SqlitePool {
    let pool = sqlx::SqlitePool::new("sqlite://").await.expect("in-memory sqlite pool failed");

    let mut conn = pool.acquire().await.expect("could not acquire db conn");

    sqlx::query_file!("migration/schema.sql").execute(&mut conn).await.expect("schema error");
    sqlx::query_file!("test/unit_test.sql").execute(&mut conn).await.expect("schema error");

    pool
}
